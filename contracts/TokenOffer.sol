pragma solidity ^0.4.11;

import "./DetailedERC20.sol";
import "zeppelin-solidity/contracts/ownership/Ownable.sol";

contract TokenOffer is Ownable {
   enum StatusOptions { Created, MakerPledge, TakerPledge, Tokens, Closed, Cancelled }

   bool public canReceiveEther;
   uint256 public numTokens;
   uint256 public price;
   DetailedERC20 public token;
   bool public isSale;
   uint256 public makerPledgeEth;
   uint256 public takerPledgeEth;
   address public maker;
   address public taker;
   StatusOptions public status;

   event Refund(uint256 price);
   event TokenSold(address buyer, address seller, uint256 price, uint256 amount);
   event TokenWithdrawn(address offer, address taker, uint256 amount);
   event Cancelled(uint256 price);

   modifier notClosed() {
      require(status != StatusOptions.Closed);
      _;
   }

   modifier canNotReceiveEther() {
      require(!canReceiveEther);
      _;
   }

   modifier hasTokens() {
      require(amount() > 0);
      _;
   }

   function TokenOffer(DetailedERC20 _token, uint256 _numTokens, uint256 _price, bool _isSale) {
      require(_numTokens > 0);
      require(_price > 0);

      token = _token;
      numTokens = _numTokens;
      price = _price;
      isSale = _isSale;
      canReceiveEther = true;
   }


   function balanceInWei() view returns(uint) {
      return this.balance;
   }

   function amount() constant returns(uint256) {
      return token.balanceOf(this);
   }

   function makerPledge(address creator) payable {
      if(isSale) {
         if(msg.value != price/10) revert();
      } else {
         if(msg.value != price) revert();
      }

      maker = creator;
      makerPledgeEth = msg.value;
      status = StatusOptions.MakerPledge;
   } 

   function takerPledge(address creator) payable {
      require(status == StatusOptions.MakerPledge);

      // when you take an offer, you send the full price on sale offer.
      // if its a purchase offer, you send 1/10 of value.
      if(isSale) {
         require(msg.value == price);
      } else {
         require(msg.value == price/10);
      }

      taker = creator;
      takerPledgeEth = msg.value;
      status = StatusOptions.TakerPledge;
   }

   function approveTokens() notClosed returns(bool) {
      address _seller = msg.sender;
      uint256 _allowedTokens = token.allowance(_seller, address(this));

      status = StatusOptions.Tokens;

      if(!token.approve(address(this), numTokens)) revert();

      TokenSold(owner, _seller, takerPledgeEth, numTokens);

      return true;
   }

   function sendTokens() notClosed returns(bool) {
      address _seller = msg.sender;
      uint256 _allowedTokens = token.allowance(_seller, address(this));

      status = StatusOptions.Tokens;

      if(!token.transferFrom(_seller, address(this), numTokens)) revert();
      if(!token.approve(taker, numTokens)) revert();
   
      // uint256 _priceInWei = priceInWei();
      _seller.transfer(takerPledgeEth);

      TokenSold(owner, _seller, takerPledgeEth, numTokens);

      return true;
   }

   function withdraw() notClosed returns(bool) {
      address _taker = msg.sender;
      uint256 _allowedTokens = token.allowance(owner, address(this));

      status = StatusOptions.Closed;

      if (_allowedTokens != numTokens) {
         revert();
      }

      if(!token.transferFrom(owner, _taker, numTokens)) revert();

      uint256 totalEth = makerPledgeEth + takerPledgeEth;

      owner.transfer(totalEth);
   
      TokenWithdrawn(owner, _taker, numTokens);

      return true;
   }

   function refund() onlyOwner notClosed returns(bool) {
      status = StatusOptions.Closed;

      // uint256 _priceInWei = priceInWei();
      uint256 _priceInWei = balanceInWei();
      owner.transfer(_priceInWei);
      Refund(_priceInWei);
      return true;
   }

  function cancel() onlyOwner notClosed returns(bool) {
    require(status != StatusOptions.Cancelled);

    StatusOptions prev_status = status;
    status = StatusOptions.Cancelled;

    uint256 _balanceInWei = balanceInWei();

    if (prev_status == StatusOptions.MakerPledge) {
      owner.transfer(_balanceInWei);
    }

    if (prev_status == StatusOptions.TakerPledge) {
      uint256 _allowedTokens = token.allowance(owner, address(this));

      if (_allowedTokens > 0) {
        revert();
      }
      else {
        if (msg.sender == maker) {
          taker.transfer(_balanceInWei);
        }
        else {
          maker.transfer(_balanceInWei);
        }
      }
    }

    Cancelled(_balanceInWei);

    return true;
  }
}
