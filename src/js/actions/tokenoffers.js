import BigNumber from 'bignumber.js'
import FlashActions from './flash'
import AccountActions from './accounts'
import FetchingActions from './fetching'
import * as ActionTypes from '../actiontypes'
import { GAS } from '../constants'
import { ERC20, OffersFactory, TokenOffer } from '../contracts'
import toWei from '../helpers/toWei'
import appendWei from '../helpers/appendWei'
import fromWei from '../helpers/fromWei'
import toTokens from '../helpers/toTokens'
import fromTokens from '../helpers/fromTokens'
import fire from '../../fire';
import Network from '../network'

const TokenOfferActions = {

  findAll() {
    return async function(dispatch) {
      try {
        const factory = await OffersFactory.deployed()
        const events = factory.TokenOfferCreated({}, { fromBlock: 0, toBlock: 'latest' });
        events.watch(function(error, result) {
          if(error) FlashActions.error(error)
          else dispatch(TokenOfferActions.loadToList(result.args.offerAddress))
        })
      } catch(error) {
        dispatch(FlashActions.error(error))
      }
    }
  },

  find(tokenOfferAddress) {
    return async function(dispatch) {
      try {
        dispatch({ type: ActionTypes.FINDING_TOKEN_OFFER })
        dispatch(FetchingActions.start('Loading token offer data'))
        const tokenOffer = await TokenOffer.at(tokenOfferAddress)
        dispatch(TokenOfferActions.load(tokenOffer))
      } catch(error) {
        dispatch(FlashActions.error(error, `There was an error trying to load given token offer contract at ${tokenSaleAddress}`))
      }
    }
  },

  create(erc20Address, seller, amount, pricePerTokenInEther) {
    return async function(dispatch) {
      dispatch(FetchingActions.start('Creating your token offer contract'))
      try {
         const erc20 = await ERC20.at(erc20Address)
         const price = new BigNumber(pricePerTokenInEther).times(amount)
         const factory = await OffersFactory.deployed()
         const decimals = await erc20.decimals()

         const transaction = await factory.createOffer(erc20Address,
               toTokens(amount, decimals), toWei(price), true,
               { from: seller, gas: GAS, value: toWei(price/10) })
         const tokenOfferAddress = transaction.logs[0].args.offerAddress;
         const tokenOffer = await TokenOffer.at(tokenOfferAddress)

         await fire.database().ref('offers').push(
               {intention: 'sale', account: seller, contractAddress: tokenOfferAddress,
               erc20: erc20Address, status: 'open', price} );

         dispatch(AccountActions.updateEtherBalance(seller))
         dispatch(AccountActions.updateTokensBalance(seller, erc20Address))
         dispatch(TokenOfferActions.loadToList(tokenOfferAddress))
         dispatch(AccountActions.notifyContractDeployed(tokenOfferAddress))
      } catch (error) {
        dispatch(FlashActions.error(error))
      }
    }
  },

   // Accept a given token sale offer.
   // Must send in whole ether in advance.
   accept(offerAddress, accepter) {
      return async function(dispatch) {
         dispatch(FetchingActions.start('Accepting a Token Offer'))
         try {
            const tokenOffer = await TokenOffer.at(offerAddress)
            const price = await tokenOffer.price()
            const erc20Address = await tokenOffer.token()

            await tokenOffer.takerPledge(accepter, { from: accepter, value: price })

            // update the table record for this offer.
            await fire.database().ref('offers').push(
                  {intention: 'purchase', account: accepter, contractAddress: offerAddress,
                  erc20: erc20Address, status: 'accepted', price} );

            dispatch(AccountActions.updateEtherBalance(accepter))
            dispatch(TokenOfferActions.load(tokenOffer))
            // dispatch(TokenOfferActions.loadToList(offerAddress))
            // dispatch(AccountActions.notifyContractDeployed(offerAddress))
            dispatch(FlashActions.success('Contract Accepted Successfully'))
         } catch (error) {
            dispatch(FlashActions.error(error))
         }
      }
   },

   fulfill(tokenOfferAddress, seller) {
      return async function(dispatch) {
         dispatch(FetchingActions.start('Fulfilling token offer contract'))
         try {
            const tokenOffer = await TokenOffer.at(tokenOfferAddress)
            const erc20Address = await tokenOffer.token()
            const erc20 = await ERC20.at(erc20Address)
            const decimals = await erc20.decimals()
            const numTokens = await tokenOffer.numTokens()
            dispatch(FetchingActions.start(`Approving ${fromTokens(numTokens, decimals)} tokens to the token purchase contract`))
            await erc20.approve(tokenOffer.address, numTokens, { from: seller, gas: GAS })
            dispatch(FetchingActions.start('Claiming your ether to the token purchase contract'))
            //await tokenOffer.approveTokens({ from: seller, gas: GAS })
            dispatch(AccountActions.updateEtherBalance(seller))
            dispatch(AccountActions.updateTokensBalance(seller, erc20Address))
            dispatch(TokenOfferActions.load(tokenOffer))
         } catch (error) {
            dispatch(FlashActions.error(error))
         }
      }
   },

   withdraw(tokenOfferAddress, taker) {
      return async function(dispatch) {
         dispatch(FetchingActions.start('Withdrawing tokens from contract'))
         try {
            const tokenOffer = await TokenOffer.at(tokenOfferAddress)
            const erc20Address = await tokenOffer.token()
            dispatch(FetchingActions.start('Claiming your tokens from the contract'))
            await tokenOffer.withdraw({ from: taker, gas: GAS })
            dispatch(AccountActions.updateEtherBalance(taker))
            dispatch(AccountActions.updateTokensBalance(taker, erc20Address))
            dispatch(TokenOfferActions.load(tokenOffer))
         } catch (error) {
            dispatch(FlashActions.error(error))
         }
      }
   },

   refund(tokenSaleAddress, seller) {
      return async function(dispatch) {
         dispatch(FetchingActions.start('Refunding your token sale contract'))
         try {
            const tokenSale = await TokenSale.at(tokenSaleAddress)
            const erc20Address = await tokenSale.token()
            await tokenSale.refund({ from: seller, gas: GAS })
            dispatch(AccountActions.updateEtherBalance(seller))
            dispatch(AccountActions.updateTokensBalance(seller, erc20Address))
            dispatch(TokenSaleActions.load(tokenSale))
         } catch (error) {
            dispatch(FlashActions.error(error))
         }
      }
   },

  loadToList(tokenOfferAddress) {
    return async function(dispatch) {
      try {
        const tokenOffer = await TokenOffer.at(tokenOfferAddress)
        const information = await TokenOfferActions._buildTokenOfferInformation(tokenOffer);

        dispatch(TokenOfferActions.add(information))
        /*if(information.closed) {
          dispatch(TokenSaleActions.loadRefundInformation(tokenSale, information, ActionTypes.ADD_TOKEN_SALE))
          dispatch(TokenSaleActions.loadPurchaserInformation(tokenSale, information, ActionTypes.ADD_TOKEN_SALE))
        }*/
      } catch(error) {
        dispatch(FlashActions.error(error))
      }
    }
  },

  load(tokenOffer) {
    return async function(dispatch) {
      try {
        const information = await TokenOfferActions._buildTokenOfferInformation(tokenOffer);
        dispatch(TokenOfferActions.receive(information))
        /*if(information.closed) {
          dispatch(TokenSaleActions.loadRefundInformation(tokenSale, information, ActionTypes.RECEIVE_TOKEN_SALE))
          dispatch(TokenSaleActions.loadPurchaserInformation(tokenSale, information, ActionTypes.RECEIVE_TOKEN_SALE))
        }*/
        dispatch(FetchingActions.stop())
      } catch(error) {
        dispatch(FlashActions.error(error))
      }
    }
  },

  loadPurchaserInformation(tokenSale, tokenSaleInformation, actionType) {
    return dispatch => {
      const events = tokenSale.TokenPurchased({}, { fromBlock: 0, toBlock: 'latest' });
      events.watch(function (error, result) {
        if (error) FlashActions.error(error)
        else {
          tokenSaleInformation.purchaser = result.args.buyer
          tokenSaleInformation.amount = fromTokens(result.args.amount, tokenSaleInformation.tokenDecimals)
          tokenSaleInformation.transactionHash = result.transactionHash
          dispatch({ type: actionType, tokenSale: tokenSaleInformation })
        }
      })
    }
  },

  loadRefundInformation(tokenSale, tokenSaleInformation, actionType) {
    return dispatch => {
      const events = tokenSale.Refund({}, { fromBlock: 0, toBlock: 'latest' });
      events.watch(function (error, result) {
        if (error) FlashActions.error(error)
        else {
          tokenSaleInformation.refunded = true
          tokenSaleInformation.amount = fromTokens(result.args.amount, tokenSaleInformation.tokenDecimals)
          tokenSaleInformation.transactionHash = result.transactionHash
          dispatch({ type: actionType, tokenSale: tokenSaleInformation })
        }
      })
    }
  },

  cancel(tokenOfferAddress, sender) {
    return async function(dispatch) {
      dispatch(FetchingActions.start('Cancelling Token Offer'))
      try {
         const tokenOffer = await TokenOffer.at(tokenOfferAddress)
         const erc20Address = await tokenOffer.token()
         await tokenOffer.cancel({ from: sender, gas: GAS })
         dispatch(AccountActions.updateEtherBalance(sender))
         dispatch(AccountActions.updateTokensBalance(sender, erc20Address))
         dispatch(TokenOfferActions.load(tokenOffer))
      } catch (error) {
         dispatch(FlashActions.error(error))
      }
   }
  },

  receive(tokenOffer) {
    return { type: ActionTypes.RECEIVE_TOKEN_OFFER, tokenOffer }
  },

  add(tokenOffer) {
    return { type: ActionTypes.ADD_TOKEN_OFFER, tokenOffer }
  },

  async _buildTokenOfferInformation(tokenOffer) {
    const erc20Address = await tokenOffer.token()
    const erc20 = await ERC20.at(erc20Address)
    const decimals = await erc20.decimals()
    const maker = await tokenOffer.owner()
    return {
      tokenDecimals: decimals,
      tokenName: await erc20.name(),
      tokenSymbol: await erc20.symbol(),
      address: tokenOffer.address,
      maker,
      taker: await tokenOffer.taker(),
      isSale: await tokenOffer.isSale(),
      numTokens: fromTokens(await tokenOffer.numTokens(), decimals), 
      //closed: await tokenOffer.closed(),
      status: await tokenOffer.status(),
      amount: fromTokens(await tokenOffer.amount(), decimals),
      price: fromWei(await tokenOffer.price()),
      tokenAddress: await tokenOffer.token(),
      balance: await Network.getBalance(tokenOffer.address),
      allowance: await erc20.allowance(maker, tokenOffer.address)
    };
  },
}

export default TokenOfferActions
