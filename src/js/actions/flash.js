import FetchingActions from './fetching'
import * as ActionTypes from '../actiontypes';

const FlashActions = {
   error(error, message = null) {
      console.error(error)
      return dispatch => {
         dispatch({ type: ActionTypes.SHOW_FLASH,
                  flash: {message: (message || error.message), type: 'error'} })
         dispatch(FetchingActions.stop())
      }
   },

   success(message) {
      return dispatch => {
         dispatch({ type: ActionTypes.SHOW_FLASH, flash: {message, type: 'success'} })
         dispatch(FetchingActions.stop())
      }
   },

   reset() {
      return dispatch => dispatch({ type: ActionTypes.RESET_FLASH })
   }
}

export default FlashActions
