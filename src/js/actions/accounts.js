import Network from '../network'
import FlashActions from './flash'
import { ERC20 } from '../contracts'
import * as ActionTypes from '../actiontypes'
import fromTokens from "../helpers/fromTokens";
import fromWei from "../helpers/fromWei";
import Store from '../store';
import FetchingActions from './fetching';

const AccountActions = {
  findCurrent() {
    return async function(dispatch) {
      try {
         let mainAddress = Store.getState().account.address;
         if (mainAddress == '') {
            const addresses = await Network.getAccounts()
            mainAddress = addresses[0]
         }
        
        dispatch(AccountActions.receive(mainAddress))
        dispatch(AccountActions.updateEtherBalance(mainAddress))
      } catch(error) {
        dispatch(FlashActions.error(error))
      }
    }
  },

  updateEtherBalance(address) {
    return async function(dispatch) {
      try {
        const balance = await Network.getBalance(address);
        const etherBalance = fromWei(balance);
        dispatch(AccountActions.receiveEtherBalance(etherBalance))
      } catch(error) {
        dispatch(FlashActions.error(error))
      }
    }
  },

   selectAccount(address) {
      return async function(dispatch) {
         try {
            dispatch(FetchingActions.start('Setting account address'))
            dispatch(AccountActions.receive(address))
            dispatch(AccountActions.updateEtherBalance(address))
            dispatch(FetchingActions.stop())
         } catch(error) {
            dispatch(FlashActions.error(error))
         }
      }
   },

  updateTokensBalance(owner, erc20Address) {
    return async function(dispatch) {
      try {
        const erc20 = await ERC20.at(erc20Address)
        const tokens = await erc20.balanceOf(owner)
        const symbol = await erc20.symbol()
        const decimals = await erc20.decimals()
        const tokensBalance = { symbol: symbol, decimals: decimals, amount: fromTokens(tokens, decimals) }
        dispatch(AccountActions.receiveTokenBalance(tokensBalance))
      } catch (error) {
        dispatch(FlashActions.error(error))
      }
    }
  },

  receive(address) {
    return { type: ActionTypes.RECEIVE_ACCOUNT, address }
  },

  notifyContractDeployed(address) {
    return { type: ActionTypes.DEPLOYED_NEW_CONTRACT, address }
  },

  resetContractDeployed() {
    return { type: ActionTypes.DEPLOYED_NEW_CONTRACT_RESET }
  },

  receiveEtherBalance(etherBalance) {
    return { type: ActionTypes.RECEIVE_ETHER_BALANCE, etherBalance }
  },

  receiveTokenBalance(tokensBalance) {
    return { type: ActionTypes.RECEIVE_TOKEN_BALANCE, tokensBalance }
  },
}

export default AccountActions
