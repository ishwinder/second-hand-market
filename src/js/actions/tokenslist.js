import fire from '../../fire';
import * as ActionTypes from '../actiontypes'

const TokensList = {

   // Find all tokens on the exchange.
   findAll() {
     return async function(dispatch) {
       try {
         let tokens = await fire.database().ref('tokens').once('value');

         dispatch(TokensList.addTokens(tokens.val()))
       } catch(error) {
         // dispatch(ErrorActions.show(error))
       }
     }
   },

   addTokens(tokensList) {
      return { type: ActionTypes.ADD_TOKENS, tokensList }
   },
}

export default TokensList;