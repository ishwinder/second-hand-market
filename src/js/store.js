import React from 'react'
import thunkMiddleware from 'redux-thunk'
import flash from './reducers/flash'
import network from './reducers/network'
import fetching from './reducers/fetching'
import account from './reducers/accounts'
import tokenSale from './reducers/tokensales'
import tokenSalesList from './reducers/tokensaleslist'
import tokenPurchase from './reducers/tokenpurchases'
import tokenPurchasesList from './reducers/tokenpurchaseslist'
import tokenOffer from './reducers/tokenoffers'
import tokensList from './reducers/tokenslist'
import tokenOffersList from './reducers/tokenofferslist'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { reducer as formReducer } from 'redux-form'
import DevTools from "./devtools";

const mainReducer = combineReducers({
  flash,
  network,
  account,
  fetching,
  tokenSale,
  tokenSalesList,
  tokenPurchase,
  tokenPurchasesList,
  tokensList,
  tokenOffer,
  tokenOffersList,
  form: formReducer
});

const enhancer = compose(
   // Add middlewares you want to use in development:
   // applyMiddleware(d1, d2, d3),
   applyMiddleware(thunkMiddleware),
   DevTools.instrument()
 );

const Store = createStore (
  mainReducer,
  {},
  enhancer
);

export default Store;
