"use strict";

import React, { Component } from "react";
import { reduxForm, Field } from 'redux-form';

export const renderTextField = ({input, label, meta:{ touched, error }, ...inputProps }) => (
   <div>
      <label>{label}</label>
      <input
            {...inputProps}
            value={input.value}
            onChange={input.onChange}
         />
      {touched && error && <span className={custom.errors}>{error}</span>}
   </div>
)

export const renderHiddenField = ({input, label, meta:{ touched, error }, ...inputProps }) => (
   <div>
      <input type="hidden"
            {...inputProps}
            value={input.value}
            onChange={input.onChange}
         />
      {touched && error && <span className={custom.errors}>{error}</span>}
   </div>
)

export const renderTextArea = ({input, label, meta:{ touched, error }, ...inputProps }) => (
   <div>
      <label>{label}</label>
      <textarea
         onChange={input.onChange}
         onBlur={input.onBlur} 
         {...inputProps}
         value={input.value}
      />
      {touched && error && <span className={custom.errors}>{error}</span>}
   </div>
)

export const renderSelectField = ({ input,options, label, className, meta: { touched, error } }) => (
    <div>
        <label>{label}</label>
        <select {...input} className={className}>
            {options}
        </select>
        {touched && error && <div className={custom["errors"]} >{error}</div>}
    </div>
)