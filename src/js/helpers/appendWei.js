import Network from "../network";

export default function appendWei(amount) {
  return Network.web3().toWei(amount, 'wei')
}
