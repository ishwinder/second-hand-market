import {
  TOKEN_OFFER_MAKER,
  TOKEN_OFFER_TAKER,
  TOKEN_OFFER_TOKENS,
  TOKEN_OFFER_CLOSED,
  TOKEN_OFFER_CANCELLED
} from "../components/token-offer/constants";

export default function contractStatusToString(contract) {
  if(!contract) return ''

  let status = 'open'
  if(contract.refunded) status = 'refunded'
  switch(contract.status.toNumber()) {
    case TOKEN_OFFER_MAKER:
      status = 'pledged'
      break
    case TOKEN_OFFER_TAKER:
      status = 'pledged'
      if (contract.allowance > 0) status = 'tokens'
      break
    case TOKEN_OFFER_CLOSED:
      status = 'closed'
      break
    case TOKEN_OFFER_CANCELLED:
      status = 'cancelled'
      break
  }

  return status
}
