import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Field, reduxForm, formValueSelector } from 'redux-form';
import Store from '../../store'
import ERC20List from '../ERC20List.react'
import AccountActions from '../../actions/accounts'
import TokenOfferActions from '../../actions/tokenoffers'
import * as FormHelper from '../../helpers/form-helper'

class TokenOfferForm extends Component {
  constructor(props){
    super(props)
    //this.state = { token: '', amount: 0, price: 0 }
    this._selectERC20 = this._selectERC20.bind(this)
    this._handleSubmit = this._handleSubmit.bind(this)
  }

   componentWillReceiveProps(nextProps) {
      this.setState({ account: nextProps.account })
   }

  render() {
    return (
      <div className={`col ${this.props.col}`}>
        <form className="card" onSubmit={this._handleSubmit}>
          <div className="card-content">
            <h3 className="title">Sell tokens</h3>
            <p>Please enter a token address manually or select the token you want to sell</p>
            <div className="row">
              <div className="input-field col s6">
               <Field name="token"
                  type="text"
                  component={FormHelper.renderTextField}
                  label="Token"
               />
 
              </div>
              <ERC20List selectERC20={this._selectERC20} col="s6"/>
            </div>
            <div className="row">
              <div className="input-field col s3">
                  <Field name="amount"
                  type="text"
                  label="Amount of Tokens to Sell"
                  component={FormHelper.renderTextField}
               />
              </div>
              <div className="input-field col s3">
              <Field name="price"
                  type="text"
                  label="Ether Price Per Token"
                  component={FormHelper.renderTextField}
               />
              </div>
            </div>
          </div>
          <div className="card-action">
            <button className="btn btn-primary">Publish</button>
          </div>
        </form>
      </div>
    )
  }

  _handleSubmit(e) {
    e.preventDefault()
    const state = this.state
    this.props.dispatch(TokenOfferActions.create(this.props.token, this.props.account.address,
         this.props.amount, this.props.price))
  }

   _selectERC20(erc20Address) {
      this.props.dispatch(AccountActions.updateTokensBalance(this.props.account.address,
         erc20Address))
      this.props.change('token', erc20Address)
   }
}

// Retrieve data from store as props
function mapStateToProps(state, props) {
   const selector = formValueSelector('tokenOfferForm')

   return {
      account: state.account,
      token: selector(state, 'token'),
      price: selector(state, 'price'),
      amount: selector(state, 'amount')
   };
}

TokenOfferForm = connect(mapStateToProps)(TokenOfferForm);
TokenOfferForm = reduxForm({
      form: 'tokenOfferForm',
      enableReinitialize: true,
      initialValues: {token: '', price: '', amount: ''} 
})(TokenOfferForm)
 

export default TokenOfferForm;
