import React from 'react'
import Store from '../../store'
import { Link } from 'react-router-dom'
import TokenOfferActions from '../../actions/tokenoffers'
import contractStatusToString from '../../helpers/contractStatusToString'

export default class TokenOffersList extends React.Component {
  constructor(props){
    super(props)
    this.state = { tokenOffers: [] }
  }

  componentDidMount() {
    Store.subscribe(() => this._onChange())
    Store.dispatch(TokenOfferActions.findAll())
  }

  render() {
    return (
      <div ref="tokenOffersList" className={"col " + this.props.col}>
        <div className="card">
          <div className="card-content">
            <h3 className="title">Token Offers</h3>
            { this.state.tokenOffers.length === 0 ? <em>Loading...</em> : <ul className="collection">{this._buildList()}</ul>}
          </div>
        </div>
      </div>
    )
  }

  _buildList() {
    return this.state.tokenOffers.map(tokenOffer => {
      const status = contractStatusToString(tokenOffer)
      return (
        <li className="collection-item" key={tokenOffer.address}>
          <div className="row">
            <div className="col s10">
              <Link className="truncate" to={`/token-offer/${tokenOffer.address}`}>
                <b>{tokenOffer.tokenSymbol} {tokenOffer.amount.toString()} - ETH {tokenOffer.price.toString()} </b> @ {tokenOffer.address}
              </Link>
            </div>
            <div className="col s2">
              <span className={`chip secondary-content ${status}-chip`}>{status}</span>
            </div>
          </div>
        </li>
      )
    })
  }

  _onChange() {
    if(this.refs.tokenOffersList) {
      const state = Store.getState();
      if(state.tokenOffersList !== this.state.tokenOffers) {
         this.setState({ tokenOffers: state.tokenOffersList });
      }
    }
  }
}
