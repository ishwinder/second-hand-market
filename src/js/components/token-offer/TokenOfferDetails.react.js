import React from 'react'
import Store from '../../store'
import Network from '../../../js/network'
import { Link } from 'react-router-dom'
import TokenOfferActions from '../../actions/tokenoffers'
import contractStatusToString from '../../helpers/contractStatusToString'
import fromTokens from '../../helpers/fromTokens'
import {
   TOKEN_OFFER_CREATED,
   TOKEN_OFFER_MAKER,
   TOKEN_OFFER_TAKER,
   TOKEN_OFFER_TOKENS,
   TOKEN_OFFER_CLOSED,
   TOKEN_OFFER_CANCELLED
} from './constants'

export default class TokenOfferDetails extends React.Component {
   constructor(props){
      super(props)
      this.state = { tokenOffer: this.props.tokenOffer, account: this.props.account }
      this._withdraw = this._withdraw.bind(this)
      this._accept = this._accept.bind(this)
      this._send = this._send.bind(this)
      this._cancel = this._cancel.bind(this)
   }

  componentWillReceiveProps(nextProps) {
    this.setState({ tokenOffer: nextProps.tokenOffer, account: nextProps.account})
  }

   render() {
      const account = this.state.account || {}
      const tokenOffer = this.state.tokenOffer
      const status = contractStatusToString(tokenOffer)
      const isMaker = tokenOffer.maker === account.address
      const isTaker = tokenOffer.taker === account.address
      const isSale = tokenOffer.isSale
      const isCancellable = (tokenOffer) => {
        const status = contractStatusToString(tokenOffer)

        if (status == 'tokens' || status == 'closed' || status == 'cancelled') {
          return false
        }

        return true
      }

      return (
      <div ref="tokenOfferDetails" className={"col " + this.props.col}>
         <div className="card">
            <div className="card-content">
            <div className="row valign-wrapper">
               <div className="col s10 valign">
                  <h3 className="title">{tokenOffer.tokenSymbol} token sale</h3>
               </div>
               <div className="col s2 valign">
                  <span className={`chip ${status}-chip`}>{status}</span>
               </div>
            </div>
            <div className="row">
               <div className="input-field col s6">
                  <label className="active">Token Sale</label>
                  <p className="labeled">{tokenOffer.address}</p>
               </div>
               <div className="input-field col s6">
                  <label className="active">Seller</label>
                  <p className="labeled">{tokenOffer.maker}</p>
               </div>
               { tokenOffer.transactionHash ?
                  <div className={"input-field col " + (tokenOffer.purchaser ? 's6' : 's12 ')}>
                  <label className="active">Confirmed Transaction</label>
                  <p className="labeled">
                     <a href={`https://ropsten.etherscan.io/tx/${tokenOffer.transactionHash}`} target="blank">{tokenOffer.transactionHash}</a>
                  </p>
                  </div> : ''
               }
               { tokenOffer.purchaser ?
                  <div className="input-field col s6">
                  <label className="active">Purchaser</label>
                  <p className="labeled">{tokenOffer.purchaser}</p>
                  </div> : ''
               }
               <div className="input-field col s3">
                  <label className="active">Selling amount of tokens</label>
                  <p className="labeled">{tokenOffer.numTokens.toString()}</p>
               </div>
               <div className="input-field col s3">
                  <label className="active">Total Ether you are willing to pay</label>
                  <p className="labeled">{tokenOffer.price.toString()}</p>
               </div>
               <div className="input-field col s3">
                  <label className="active">Token Name</label>
                  <p className="labeled">{tokenOffer.tokenName}</p>
               </div>
               <div className="input-field col s3">
                  <label className="active">Token Symbol</label>
                  <p className="labeled">{tokenOffer.tokenSymbol}</p>
               </div>
               <div className="input-field col s3">
                  <label className="active">ETH Balance</label>
                  <p className="labeled">{tokenOffer.balance.toNumber()/Math.pow(10, 18)}</p>
               </div>
               <div className="input-field col s3">
                  <label className="active">Token Balance</label>
                  <p className="labeled">{tokenOffer.amount.toNumber()}</p>
               </div>
               <div className="input-field col s12">
                  <label className="active">Sharing Link</label>
                  <p className="labeled">
                  <Link to={`/token-offer/${tokenOffer.address}`}>{window.location.origin}/token-offer/{tokenOffer.address}</Link>
                  </p>
               </div>
            </div>
            </div>
            <div className="card-action">
               {isSale && !isMaker && tokenOffer.status == TOKEN_OFFER_MAKER &&
               <button className="btn btn-success"
                     disabled={tokenOffer.closed}
                     onClick={this._accept}
                     style={{marginRight: '6px'}}>Accept</button>
               }
               {isSale && isMaker && tokenOffer.status == TOKEN_OFFER_TAKER &&
                  tokenOffer.approval == 0 &&
                <button className="btn btn-success"
                     onClick={this._send}>Send Tokens</button>
               }
               {isSale && isTaker && contractStatusToString(tokenOffer) == 'tokens' &&
               <button className="btn btn-success"
                  disabled={tokenOffer.closed}
                  onClick={this._withdraw}>Withdraw Tokens</button>
               }
               {
                isSale && isCancellable(tokenOffer) &&
                <button className="btn btn-alert"
                  disabled={tokenOffer.closed}
                  onClick={this._cancel}>Cancel Offer</button>
               }
               {isSale && isMaker && contractStatusToString(tokenOffer) == 'tokens' &&
                <span>Waiting for Withdrawal</span>
               }
            </div>
         </div>
      </div>
      );
   }

   _withdraw(e) {
      e.preventDefault()
      const tokenOffer = this.state.tokenOffer;
      Store.dispatch(TokenOfferActions.withdraw(tokenOffer.address,
            this.state.account.address))
   }

   _accept(e) {
      e.preventDefault()
      const tokenOffer = this.state.tokenOffer;
      Store.dispatch(TokenOfferActions.accept(tokenOffer.address, this.state.account.address))
   }

   _send(e) {
      e.preventDefault()
      const tokenOffer = this.state.tokenOffer;
      Store.dispatch(TokenOfferActions.fulfill(tokenOffer.address, this.state.account.address))
   }

   _cancel(e) {
      e.preventDefault()
      const tokenOffer = this.state.tokenOffer;
      Store.dispatch(TokenOfferActions.cancel(tokenOffer.address, this.state.account.address))
    }
}
