import React from 'react'
import App from './App.react'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import DevTools from "../devtools";

import createBrowserHistory from 'history/createBrowserHistory'
const browserHistory = createBrowserHistory()

const Root = ({ store }) => (
  <Provider store={store}>
    <div>
      <Router history={browserHistory}>
         <App/>
      </Router>
      <DevTools />
    </div>
  </Provider>
)

export default Root
