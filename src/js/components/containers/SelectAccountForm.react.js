import React, {Component} from 'react';
import {connect} from 'react-redux';
import Store from '../../store'
import AccountActions from '../../actions/accounts'

class SelectAccountForm extends Component {
   constructor(props){
      super(props)
      this.state = { address: '' }

      this._handleSubmit = this._handleSubmit.bind(this)
      this._updateAddress = this._updateAddress.bind(this)
      this._onChange = this._onChange.bind(this)
   }

   componentDidMount() {
      //Store.subscribe(() => this._onChange())
   }

   render() {
      return (
         <div ref="selectAddressForm" className={`col ${this.props.col}`}>
            <form className="card" onSubmit={this._handleSubmit}>
               <div className="card-content">
                  <h3 className="title">Select Address</h3>
                  <p>Please enter an address to use</p>
                  <div className="row">
                     <div className="input-field col s6">
                        <label>Address</label>
                        <input type="text"
                           onChange={this._updateAddress}
                           value={this.state.address} 
                           required
                        />
                     </div>
                  </div>
               </div>
               <div className="card-action">
                  <button className="btn btn-primary">Select</button>
               </div>
            </form>
         </div>
      )
   }

   _handleSubmit(e) {
      e.preventDefault()

      this.props.dispatch(AccountActions.selectAccount(this.state.address))
   }

   _updateAddress(e) {
      e.preventDefault();
      this.setState({ address: e.target.value })
  }

  _onChange() {
      if(this.refs.selectAddressForm) {
         const state = Store.getState()

         if(state.account.address) {
            this.props.history.push('/')
         }
      }
   }
}

SelectAccountForm = connect()(SelectAccountForm);

export default SelectAccountForm;

