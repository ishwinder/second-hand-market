import React, { Component } from 'react'
import Store from '../../store'
import {connect} from 'react-redux';
import Account from '../Account.react'
import AccountActions from '../../actions/accounts'
import TokenOfferForm from '../token-offer/TokenOfferForm.react';

class NewTokenOffer extends Component {
  constructor(props){
    super(props)
    // this.state = { account: {} }
  }

   componentDidMount() {
      if (this.props.account.address == '') {
         this.props.dispatch(AccountActions.findCurrent())
      }
      Store.subscribe(() => this._onChange())
   }

  render() {
    const undefinedAccount = typeof this.props.account.address === 'undefined'
    return (
      <div ref="newTokenOffer">
        {undefinedAccount ? '' :
        <div className="row">
          <Account account={this.props.account} col="s12"/>
          <TokenOfferForm col="s12"/>
        </div>}
      </div>
    )
  }

   async _onChange() {
    if(this.refs.newTokenOffer) {
      const state = Store.getState()
      this.setState({ account: state.account })
      const deployedAddress = state.account.deployedAddress;
      if(deployedAddress) {
         this.props.history.push(`/token-offer/${deployedAddress}`)
         Store.dispatch(AccountActions.resetContractDeployed())
      }
    }
  }
}

// Maps state from store to props
const mapStateToProps = (state,props) => {
   return {
     account: state.account
   }
 };
 
NewTokenOffer = connect(mapStateToProps)(NewTokenOffer);

export default NewTokenOffer;