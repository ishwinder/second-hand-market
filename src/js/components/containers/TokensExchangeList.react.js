import React, { Component } from 'react';
import { connect } from 'react-redux';
import Store from '../../store'
import { Link } from 'react-router-dom'
import _ from 'lodash';

import TokensList from '../../actions/tokenslist'

class TokensExchangeList extends Component {
  constructor(props){
    super(props)
    this.state = { tokensList: {} }

    this._onChange = this._onChange.bind(this)
  }

   componentDidMount() {
      //Store.subscribe(() => this._onChange()) 
      if (_.isEmpty(this.props.tokensList)) {
         this.props.dispatch(TokensList.findAll())
      }
   }

  render() {
    return (
      <div ref="tokensList" className={"col " + this.props.col}>
        <div className="card">
          <div className="card-content">
            <h3 className="title">Available Tokens</h3>
            { Object.keys(this.props.tokensList).length === 0 ? <em>Loading...</em> : <ul className="collection">{this._buildList()}</ul>}
          </div>
        </div>
      </div>
    )
  }

  _buildList() {
    return _.values(this.props.tokensList).map(token => {

      return (
        <li className="collection-item" key={token.symbol}>
          <div className="row">
            <div className="col s10">
              <Link className="truncate" to={`/tokens/${token.symbol}`}>
                <b>{token.name} {token.symbol}</b>
              </Link>
            </div>
            <div className="col s2">
              <span className={`chip secondary-content ${status}-chip`}>{status}</span>
            </div>
          </div>
        </li>
      )
    })
  }

   _onChange() {
      if(this.refs.tokensList) {
         const state = Store.getState();
         if(state.tokensList !== this.state.tokensList) {
            this.setState({ tokensList: state.tokensList });
         }
      }
   }

}

// Maps state from store to props
const mapStateToProps = (state,props) => {
   return {
     tokensList:state.tokensList
   }
 };
 
 TokensExchangeList = connect(mapStateToProps)(TokensExchangeList);

 export default TokensExchangeList;
