import React, { Component } from 'react'
import Store from '../../store'
import {connect} from 'react-redux'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import TokensList from '../../actions/tokenslist'
import TokenPurchaseActions from '../../actions/tokenpurchases'
import fire from '../../../fire'

class TokenDeals extends Component {
   constructor(props) {
      super(props)
      this.state = { tokenPurchases: [], tokenSales: [] }
   }

   async componentDidMount() {
      //Store.subscribe(() => this._onChange()) 
      //Store.dispatch(TokensList.findAll())
      let token = await fire.database()
                        .ref('tokens')
                        .orderByChild('symbol')
                        .equalTo(this.props.match.params.token)
                        .once('value')

      if (!_.isEmpty(token)) {
         let tokenData = Object.values(token.val())[0];
         this.props.dispatch(TokenPurchaseActions.findByERC20Address(tokenData.erc20Address))
      }
   }

  render() {
    return (
      <div ref="tokensList" className={"col " + this.props.col}>
        <div className="card">
          <div className="card-content">
            <h3 className="title">Available Tokens</h3>
          </div>
        </div>
      </div>
    )
  }

  _buildList() {
    return _.values(this.state.tokensList).map(token => {

      return (
        <li className="collection-item" key={token.symbol}>
          <div className="row">
            <div className="col s10">
              <Link className="truncate" to={`/tokens/${token.symbol}`}>
                <b>{token.name} {token.symbol}</b>
              </Link>
            </div>
            <div className="col s2">
              <span className={`chip secondary-content ${status}-chip`}>{status}</span>
            </div>
          </div>
        </li>
      )
    })
  }

   _onChange() {
      if(this.refs.tokensList) {
         const state = Store.getState();
         if(state.tokensList !== this.state.tokensList) {
            this.setState({ tokensList: state.tokensList });
         }
      }
   }

}

// Retrieve data from store as props
function mapStateToProps(state, props) {
   return {
      tokenPurchases: state.tokenPurchases,
      tokenSales: state.tokenSales
   };
}

// Use connect to put them together
export default connect(mapStateToProps)(TokenDeals);
