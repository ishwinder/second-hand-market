import React from 'react';
import { Link } from 'react-router-dom'
import TokenOffersList from '../token-offer/TokenOffersList.react'
import TokensExchangeList from './TokensExchangeList.react'

export default class TokenOffers extends React.Component {
  render() {
    return (
      <div className="home">
        <div className="row buy-sell-question center">
          <h3 className="super-title">Would you like to buy or sell tokens?</h3>
          <div className="row">
            <Link to="/token-purchase" className="btn btn-large btn-primary buy-button" id="buy">Buy</Link>
            <Link to="/token-sale" className="btn btn-large btn-primary" id="sell">Sell</Link>
            <Link to="/token-offers/new" className="btn btn-large btn-primary" id="offer">New Offer</Link>
          </div>
        </div>
        <div className="row tokens-lists">
          <TokensExchangeList col="s12"/>
        </div>
        <div className="row contracts-lists">
          <TokenOffersList col="s6"/>
        </div>
      </div>
    )
  }
}
