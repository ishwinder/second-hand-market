import React from 'react'
import Store from '../../store'
import Account from '../Account.react'
import TokenOfferDetails from '../token-offer/TokenOfferDetails.react'
//import TokenSaleFulfill from '../token-sale/TokenSaleFulfill.react'
import TokenOfferActions from '../../actions/tokenoffers'
import AccountActions from "../../actions/accounts";

export default class ShowTokenOffer extends React.Component {
  constructor(props){
    super(props)
    this.state = { tokenOffer: {}, account: {} }
  }

  componentDidMount() {
    Store.subscribe(() => this._onChange())
    Store.dispatch(AccountActions.findCurrent())
    Store.dispatch(TokenOfferActions.find(this.props.match.params.address))
  }

  render() {
    const account = this.state.account
    const tokenOffer = this.state.tokenOffer
    const undefinedAccount = typeof account.etherBalance === 'undefined'
    const undefinedContract = typeof tokenOffer.amount === 'undefined'
    const loading = undefinedAccount || undefinedContract

    return (
      <div ref="tokenOffer">
        {loading ? '' :
        <div className="row">
          <Account account={account} col="s12"/>
          <TokenOfferDetails tokenOffer={tokenOffer} account={account} col="s12"/>
        </div>}
      </div>
    )
  }

  _onChange() {
    if(this.refs.tokenOffer) {
      const state = Store.getState()
      this.setState({ tokenOffer: state.tokenOffer, account: state.account })
      if(state.tokenOffer.tokenAddress && state.account.address && state.account.tokensBalance === null)
        Store.dispatch(AccountActions.updateTokensBalance(state.account.address, state.tokenOffer.tokenAddress))
    }
  }
}
