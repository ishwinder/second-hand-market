import React from 'react'
import Store from '../store'
import FlashActions from "../actions/flash";

export default class Flash extends React.Component {
  constructor(props){
    super(props)
    this.state = { message: null, type: 'error' }
    this._cleanMessage = this._cleanMessage.bind(this)
  }

  componentDidMount() {
    Store.subscribe(() => this._onChange())
  }

   render() {
      const {message, type} = this.state;
      return !message ? <div ref="message"/> :
      <div ref="message">
         <div className="row">
            <div className="col s12">
            {type == 'error' ?
               <div className="card red lighten-1">
                  <div className="card-content white-text">
                     <p className="center">{message} <span className="close-message" onClick={this._cleanMessage}>x</span></p>
                  </div>
               </div>
               :
               <div className="card green lighten-1">
                  <div className="card-content white-text">
                     <p className="center">{message} <span className="close-message" onClick={this._cleanMessage}>x</span></p>
                  </div>
               </div>
            }
            </div>
         </div>
      </div>
   }

   _cleanMessage(e) {
      e.preventDefault();
      this.setState({ message: null })
      Store.dispatch(FlashActions.reset())
   }

   _onChange() {
      if(this.refs.message) {
         const state = Store.getState()
         if (state.flash) {
            this.setState({ message: state.flash.message, type: state.flash.type })
         }
      }
   }
}
