import React, { Component } from 'react';
import Store from '../store'
import {connect} from 'react-redux';
import _ from 'lodash';

import TokensList from '../actions/tokenslist'

class ERC20List extends Component {
   constructor(props){
      super(props)
      this.selectERC20 = this.selectERC20.bind(this)
   }

   componentDidMount() {
      if (_.isEmpty(this.props.tokensList)) {
         this.props.dispatch(TokensList.findAll())
      }
   }

   selectERC20(e) {
      e.preventDefault()
      this.props.selectERC20(e.target.id)
   }

  render() {
    return (
      <div className={`col ${this.props.col} erc20-list`}>
        {
           _.values(this.props.tokensList).map((token) => {
               return (<a className="btn btn-floating rounded-btn tooltipped"
                        id={`${token.erc20Address}`}
                        data-position="bottom"
                        data-delay="50"
                        data-tooltip={`${token.name}`}
                        onClick={this.selectERC20}>{token.symbol}</a>
                     )
           })
        }
      </div>
    );
  }
}

// Retrieve data from store as props
function mapStateToProps(state, props) {
   return {
      tokensList: state.tokensList
   };
}

// Use connect to put them together
export default connect(mapStateToProps)(ERC20List);