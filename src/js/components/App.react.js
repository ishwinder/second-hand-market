import React from 'react'
import Home from './Home'
import Store from '../store'
import Modal from './Modal.react'
import Flash from './Flash.react'
import Navbar from './Navbar.react'
import NetworkActions from '../actions/network'
import NewTokenSale from './containers/NewTokenSale.react'
import ShowTokenSale from './containers/ShowTokenSale.react'
import ShowTokenPurchase from './containers/ShowTokenPurchase.react'
import NewTokenPurchase from './containers/NewTokenPurchase.react'
import TokenDeals from './containers/TokenDeals.react'
import NewTokenOffer from './containers/NewTokenOffer.react';
import { Switch, Route, Redirect } from 'react-router-dom'
import TokenOffers from './containers/TokenOffers.react';
import TokenOffersList from './token-offer/TokenOffersList.react';
import ShowTokenOffer from './containers/ShowTokenOffer.react';
import SelectAccountForm from './containers/SelectAccountForm.react';

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = { connected: null, couldAccessAccount: null, fetching: false }
  }

   componentDidMount() {
      Store.subscribe(() => this._onChange())
      Store.dispatch(NetworkActions.checkConnection())
      Store.dispatch(NetworkActions.checkAccountAccess())
   }

   render() {
      const connected = this.state.connected
      const couldAccessAccount = this.state.couldAccessAccount
      const fetching = connected && couldAccessAccount && this.state.fetching !== null
      const mainAddress = Store.getState().account.address;

      const SelectAccountRoute = ({ component: Component, ...rest }) => (
         <Route {...rest} render={(props) => (
         mainAddress != ''
            ? <Component {...props} />
            : <Redirect to='/select-account' />
         )} />
      )

    return (
      <div ref="app">
        <Navbar/>
        <div className="main-container container">
          <Flash />
          <Switch>
            <Route path="/" exact component={TokenOffers}/>
            <Route path="/select-account" exact component={SelectAccountForm} />
            <Route path="/tokens/:token" exact component={TokenDeals}/>
            <Route path="/token-sale/" exact component={NewTokenSale}/>
            <Route path="/token-sale/:address" component={ShowTokenSale}/>
            <Route path="/token-purchase/" exact component={NewTokenPurchase}/>
            <Route path="/token-purchase/:address" component={ShowTokenPurchase}/>
            <Route path="/token-offers/new" exact component={NewTokenOffer}/>
            <Route path="/token-offers" exact component={TokenOffersList}/>
            <Route path="/token-offer/:address" exact component={ShowTokenOffer}/>
          </Switch>
        </div>
        <Modal open={fetching} progressBar message={this.state.fetching}/>
        <Modal dark open={!connected} message={'Please access using MetaMask'}/>
        <Modal dark open={connected && !couldAccessAccount} message={'Please unlock your account on MetaMask'}/>
      </div>
    )
  }

   _onChange() {
      if(this.refs.app) {
         const state = Store.getState()
         this.setState({ fetching: state.fetching, connected: state.network.connected,
               couldAccessAccount: state.network.couldAccessAccount })
      }
   }
}
