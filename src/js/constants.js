export const GAS = 2000000;                                  // amount of gas to use for the transaction
export const LOCALHOST_PROVIDER = "http://localhost:8545";   // default localhost provider for Web3
