import React from 'react';
import * as ActionTypes from '../actiontypes'

const TokenOffersReducer = (state = {}, action) => {
  switch (action.type) {
      case ActionTypes.FINDING_TOKEN_PURCHASE:
         return {}
      case ActionTypes.RECEIVE_TOKEN_OFFER:
         return Object.assign({}, state, action.tokenOffer)
      default:
         return state
  }
};

export default TokenOffersReducer;
