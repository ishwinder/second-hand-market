import React from 'react';
import * as ActionTypes from '../actiontypes'

const FlashReducer = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.SHOW_FLASH:
      return action.flash;
    case ActionTypes.RESET_FLASH:
      return null;
    default:
      return state
  }
};

export default FlashReducer;
