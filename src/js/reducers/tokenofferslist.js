import React from 'react';
import * as ActionTypes from '../actiontypes'

const TokenOffersListReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.ADD_TOKEN_OFFER:
      const tokenOffer = action.tokenOffer
      const existingTokenOffer = state.find(contract => contract.address === tokenOffer.address)
      return existingTokenOffer ? state : [tokenOffer].concat(state)
    default:
      return state
  }
};

export default TokenOffersListReducer;
