import React from 'react';
import * as ActionTypes from '../actiontypes'

// List of Tokens the app is working on.
const TokensListReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.ADD_TOKENS:
      return Object.assign({}, state, action.tokensList)
    default:
      return state
  }
};

export default TokensListReducer;
